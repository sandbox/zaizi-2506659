                                              User Manual For Prediction IO Module
                                      ------------------------------------------------------

-- SUMMARY --

A module to integrate with Prediction.IO which is a well known open source Machine Learning Server and a recommendation engine.
The purpose of this module is a to build a scalable solution to get user recommendations within the data model of your website.
The module will have the following features

1. Collect user ratings and statistics
2. Predict and Recommend other nodes/entities in your website that the user might like

For a full description of the module, visit the project page:
  http://drupal.org/project/prediction_io

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/prediction_io


-- REQUIREMENTS --

-Dependencies -

Libraries : https://drupal.org/project/libraries
3rd party SDK : https://github.com/PredictionIO/PredictionIO-PHP-SDK


-- INSTALLATION --

* First you have to install libraries module as usual way
* Next you have to Download the 3rd party PHP SDK for Prediction IO and add it to the /sites/all/libraries folder
* rename SDK folder with this name "PredictionIOPHPSDK"
* on terminal go to PredictionIOPHPSDK folder and Run <composer install> to install other dependencies for SDK


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:


-- CUSTOMIZATION --



-- TROUBLESHOOTING --



-- FAQ --



-- CONTACT --

Current maintainers:

