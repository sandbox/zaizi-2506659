<?php
/**
 * Created by PhpStorm.
 * User: sathukorala
 * Date: 7/2/15
 * Time: 1:11 PM
 */


function prediction_io_statistics() {
  $node_details = $_POST['node_details'];
  $user_details = $_POST['user_details'];

  $access_key = variable_get('prediction_io_access_key');
  $server_url = variable_get('prediction_io_server_url');
  $port = variable_get('prediction_io_port_1');

  $node_id = $node_details['nid'];
  $user_id = $user_details['uid'];

try{
  $client = new \predictionio\EventClient($access_key, $server_url.':'.$port);

  /* Statistics Data */
  $response = $client->createEvent(array(
    'event' => 'rate',
    'entityType' => 'user',
    'entityId' => $user_id,
    'targetEntityType' => 'item',
    'targetEntityId' => $node_id,
    'properties' => array('rating'=> 5)
  ));
  echo json_encode($response);
} catch (\predictionio\PredictionIOAPIError $e) {
  drupal_set_message($e->getMessage() ,'error');
}
  exit();
}



