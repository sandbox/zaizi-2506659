/**
 * Created by sathukorala on 7/2/15.
 */

(function ($) {
    $(document).ready(function() {
        var callbackpath = "/data/statistics";
        $.ajax({
            type: "POST",
            cache: false,
            url: callbackpath,
            data: Drupal.settings.prediction_io.data,
            success: function (data) {
                var resultObj = eval("(" + data + ")");
                console.log(resultObj);
            }
        });
    });
})(jQuery);